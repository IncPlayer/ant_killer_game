using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_Laser : MonoBehaviour
{
    public GameObject[] laserColors;
    public int Select = 0;

    public float hitDistance;
    public GameObject prefabTest;
    public GameObject laser;

    private void Update()
    {
        Vector3 clickPos = -Vector3.one;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            clickPos = hit.point;
        }

        LaserFollow(clickPos);

        if (Input.GetMouseButton(0))
        {
            GameObject test = Instantiate(laserColors[Select], clickPos, Quaternion.identity);
            SetupVisuel(laser, true);
        }
        else
        {
            SetupVisuel(laser, false);
        }

        if (Input.GetMouseButtonDown(1))
        {
            SwitchColors();
        }
    }

    private void LaserFollow(Vector3 orientationPos)
    {
        this.gameObject.transform.LookAt(orientationPos);
    }

    private void SetupVisuel(GameObject trail, bool isActive)
    {
        trail.gameObject.SetActive(isActive);
    }

    private void SwitchColors()
    {
        Select += 1;

        if (Select == 3)
        {
            Select = 0;
        }

    }
}
